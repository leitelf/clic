#include "clic.h"

char *clic_strdup (char *dest, char *orig)
{
    if (dest != NULL) {
        dest = realloc(dest, (strlen(orig) + 1) * sizeof(char));
        
        if (dest == NULL) {
            return NULL;
        }
    } else {
        dest = malloc((strlen(orig) + 1) * sizeof(char));

        if (dest == NULL) {
            return NULL;
        }
    }

    strcpy(dest, orig);
    return dest;
}

void clic_free_args (int *argc, char **argv)
{
    while (*argc > 0) {
        (*argc)--;
        if(argv != NULL)
            if (argv[*argc] != NULL) {
                free(argv[*argc]);
                argv[*argc] = NULL;
            }
    }

    if (argv != NULL) {
        free(argv);
        argv = NULL;
    }

}

char **clic_add_arg (int *argc, char **argv, char *buffer, int cc)
{
    if (argv == NULL) {
        argv = malloc(sizeof(char *));
        if (argv == NULL) {
            *argc = 0;
            return NULL;
        }
    } else {
        argv = realloc(argv, sizeof(char *) * ((*argc) + 1));
        if (argv == NULL) {
            *argc = 0;
            return NULL;
        }
    }

    argv[*argc] = malloc(sizeof(char) * ((cc) + 1));
    if (argv[*argc] == NULL) {
        clic_free_args(argc, argv);
        return NULL;
    }

    strncpy(argv[*argc], buffer, cc);
    argv[*argc][cc] = '\0';
    (*argc)++;

    return argv;
}

char **clic_get_cl (char *buffer, int *argc)
{
    int i, begin;
    char **argv = NULL;

    short open_arg;

    if (buffer == NULL) {
        return NULL;
    }
    
    if (argc == NULL) {
        argc = malloc(sizeof(int));
    }

    *argc = 0;
    begin = 0;

    open_arg = 0;
    int strs = strlen(buffer);
    for (i = 0; i < strs; i++) {
        if (!open_arg) {
            if (buffer[i] != ' ') {
                begin = i;
                open_arg = 1;
                if (i == (strs - 1)) {
                    return clic_add_arg(argc, argv, &buffer[i], 1);
                }
            }
        }
        else if (open_arg) {
            if (buffer[i] == ' ') {
                argv = clic_add_arg(argc,
                                    argv,
                                    &buffer[begin],
                                    i - begin);
                open_arg = 0;
            } else if (i == (strs - 1)) {
                argv = clic_add_arg(argc,
                                    argv,
                                    &buffer[begin],
                                    (i+1) - begin);
            }
        }
    }

    return argv;

}

struct clic_cmd *clic_cmd_init ()
{
    struct clic_cmd *cmd = malloc(sizeof(struct clic_cmd));

    if (cmd == NULL) {
        return NULL;
    }

    cmd->name = NULL;
    cmd->usage = NULL;
    cmd->description = NULL;
    cmd->cb = NULL;

    return cmd;
}

void clic_cmd_free (struct clic_cmd *cmd)
{
    if (cmd != NULL) {
        if (cmd->name != NULL) {
            free(cmd->name);
            cmd->name = NULL;
        }
        if (cmd->usage != NULL) {
            free(cmd->usage);
            cmd->usage = NULL;
        }
        if (cmd->description != NULL) {
            free(cmd->description);
            cmd->description = NULL;
        }

        free(cmd);
        cmd = NULL;
    }
}

char *clic_cmd_set_name (struct clic_cmd *cmd, char *name)
{
    if (cmd == NULL) {
        if ((cmd = clic_cmd_init()) == NULL) {
            return NULL;
        }
    }    

    return cmd->name = clic_strdup(cmd->name, name);
}

char *clic_cmd_set_usage (struct clic_cmd *cmd, char *usage)
{
    if (cmd == NULL) {
        if ((cmd = clic_cmd_init()) == NULL) {
            return NULL;
        }
    }    

    return cmd->usage = clic_strdup(cmd->usage, usage);
}

char *clic_cmd_set_description (struct clic_cmd *cmd, char *description)
{
    if (cmd == NULL) {
        if ((cmd = clic_cmd_init()) == NULL) {
            return NULL;
        }
    }    

    return cmd->description = clic_strdup(cmd->description, description);
}

void clic_cmd_set_cb (struct clic_cmd *cmd, clic_cb cb)
{
    cmd->cb = cb;
}

struct clic_cmd_list *clic_cmd_list_init ()
{
    struct clic_cmd_list *cmd_list = malloc(sizeof(struct clic_cmd_list));
    if (cmd_list == NULL) {
        return NULL;
    }

    cmd_list->cmds = NULL;
    cmd_list->qty = 0;

    return cmd_list;
}

void clic_cmd_list_free (struct clic_cmd_list *cmd_list)
{
    if (cmd_list != NULL) {
        if (cmd_list->cmds != NULL) {
            while (cmd_list->qty > 0) {
                cmd_list->qty--;
                if (cmd_list->cmds[cmd_list->qty]){
                    clic_cmd_free(cmd_list->cmds[cmd_list->qty]);
                    cmd_list->cmds[cmd_list->qty] = NULL;
                }
                    
            }
            free(cmd_list->cmds);
            cmd_list->cmds = NULL;
        }

        free(cmd_list);
        cmd_list = NULL;
    }
}

struct clic_cmd *clic_cmd_cpy (struct clic_cmd *dest, struct clic_cmd *orig)
{
    if (dest == NULL) {
        if ((dest = clic_cmd_init()) == NULL) {
            return NULL;
        }
    }

    clic_cmd_set_name(dest, orig->name);
    clic_cmd_set_usage(dest, orig->usage);
    clic_cmd_set_description(dest, orig->description);
    clic_cmd_set_cb(dest, orig->cb);

    return dest;
}

struct clic_cmd *clic_add_cmd (struct clic_cmd_list *cmd_list, 
                               struct clic_cmd *cmd)
{
    if (cmd_list == NULL) {
        if ((cmd_list = clic_cmd_list_init()) == NULL) {
            return NULL;
        }
    }

    if (cmd_list->cmds == NULL)
        cmd_list->cmds = malloc(sizeof(struct clic_cmd *));

    else
        cmd_list->cmds = realloc(cmd_list->cmds, 
                           sizeof(struct clic_cmd *) * (cmd_list->qty + 1));
    
    if (cmd_list->cmds == NULL)
        return NULL;

    if ((cmd_list->cmds[cmd_list->qty] = clic_cmd_cpy(cmd_list->cmds[cmd_list->qty],
                                                     cmd)) != NULL) {
        cmd_list->qty++;
        return cmd_list->cmds[cmd_list->qty - 1];
    }
     

    return NULL;
}

struct clic_cmd *clic_new_cmd (struct clic_cmd_list *cmd_list,
                               char *name,
                               char *usage,
                               char *description,
                               clic_cb cb)
{
    struct clic_cmd *cmd = clic_cmd_init();
    clic_cmd_set_name(cmd, name);
    clic_cmd_set_usage(cmd, usage);
    clic_cmd_set_description(cmd, description);
    clic_cmd_set_cb(cmd, cb);

    if ( clic_add_cmd(cmd_list, cmd) == NULL) {
        clic_cmd_free(cmd);
        return NULL;
    }

    clic_cmd_free(cmd);
    return cmd_list->cmds[cmd_list->qty - 1];
}

void clic_prompt (char *prompt, struct clic_cmd_list *cmd_list)
{
    char *buffer = NULL;
    int argc;
    char **argv = NULL;
    int cmd_ret = 0;
    int cmd_used = 0;
    unsigned int i;

    if (cmd_list == NULL) {
        printf("empty command list\n");
        return;
    }

    while (1) {
        buffer = readline(prompt);
        if (strlen(buffer) > 0) {
            add_history(buffer);

            argv = clic_get_cl(buffer, &argc);

            if (argc) {
                for (i = 0; i < cmd_list->qty; i++) {
                    if (!strcmp(argv[0], cmd_list->cmds[i]->name)) {
                        cmd_ret = cmd_list->cmds[i]->cb(argc, 
                                                        argv,
                                                        cmd_list,
                                                        i);
                        cmd_used = 1;
                    }
                }
            }
            if (cmd_used && (cmd_ret == CLIC_EXIT)) {
                break;
            }
        }
    }

    if (buffer != NULL) {
        free(buffer);
        buffer = NULL;
    }

    clic_free_args(&argc, argv);
}

void clic_cmd_usage (struct clic_cmd *cmd)
{
    printf(CLIC_ANSI_BOLD "%s" CLIC_ANSI_DEFAULT, cmd->usage);
    printf(" - %s\n", cmd->description);
}

int clic_help_cb (int argc,
                  char **argv,
                  struct clic_cmd_list *cmd_list,
                  int idx)
{
    unsigned int i;

    if (argc != 1) {
        printf("invalid use of the command %s\n", argv[0]);
        clic_cmd_usage(cmd_list->cmds[idx]);
        return 1;
    }

    for (i = 0; i < cmd_list->qty; i++) {
        clic_cmd_usage(cmd_list->cmds[i]);
    }

    return 0;
}

int clic_exit_cb (int argc,
                  char **argv,
                  struct clic_cmd_list *cmd_list,
                  int idx)
{
    if (argc != 1) {
        printf("invalid use of the command %s\n", argv[0]);
        clic_cmd_usage(cmd_list->cmds[idx]);
        return 1;
    }

    printf("Bye!\n");
    return CLIC_EXIT;
}

int clic_clear_cb (int argc,
                  char **argv,
                  struct clic_cmd_list *cmd_list,
                  int idx)
{
    int i = 50;

    if (argc != 1) {
        printf("invalid use of the command %s\n", argv[0]);
        clic_cmd_usage(cmd_list->cmds[idx]);
        return 1;
    }

    while (i--) {
        printf("\n");
    }

    return 0;
}
