# CLIC

Library for easy create CLI in C language.

## Install
### Dependencies
Before proceed to library installation, be sure to have GCC compiler, MAKE 
and readline library installed.

On ubuntu (or derived distributions) you can install the dependencies with the 
following packages:
``` bash
sudo apt install build-essential
sudo apt install libreadline libreadline-dev
```

---------------------
### Download
After have installed all dependecies you can download and install the deb package on the [release](https://gitlab.com/leitelf/clic/-/releases) page, or compile the library.

### Compile
You can compile clic library using the command `make`. It will generate `lib` and `obj` folders. You can find shared and static libraries inside `lib` folder.

You can compile the test using the following command line:

``` bash
# using the shared library
gcc test.c lib/libclic.so -o test -lreadline
```

``` bash
# using the static library`
gcc test.c lib/libclic.a -o test -lreadline
```

## Issues

If you have any issue with the library, please, report it in the [repository](https://gitlab.com/leitelf/clic/issues).
