# configuration variables
CC = gcc
COMPILER_FLAGS = -Wall -Wextra -Winline -fPIC
LINKER_FLAGS = -lreadline
OBJ_DIR = ./obj
LIB_DIR = ./lib

# library folders
SHARED_FILES = $(wildcard $(LIB_DIR)/*.so)
STATIC_FILES = $(wildcard $(LIB_DIR)/*.a)

# making the necessary directories
$(shell mkdir -p $(OBJ_DIR))
$(shell mkdir -p $(LIB_DIR))

# targets to create
all: objects shared static

# compiling object files
objects: $(OBJ_DIR)/clic.o
$(OBJ_DIR)/clic.o: clic.c
	$(CC) -c $(COMPILER_FLAGS) $(LINKER_FLAGS) -c -o $(OBJ_DIR)/clic.o clic.c

# creating shared library
shared: $(OBJ_DIR)/clic.o
	$(CC) -shared -o $(LIB_DIR)/libclic.so $(OBJ_DIR)/clic.o

# creating static library
static: $(OBJ_DIR)/clic.o
	ar rcs $(LIB_DIR)/libclic.a $(OBJ_DIR)/clic.o

# cleaning the built files
clean:
	rm -f $(OBJ_DIR)/clic.o
	rm -f $(LIB_DIR)/libclic.so
	rm -f $(LIB_DIR)/libclic.a