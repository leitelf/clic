#include "./clic.h"

int main(int argc, char **argv) {
    struct clic_cmd_list *cmd_lst = clic_cmd_list_init();

    clic_new_cmd(cmd_lst,
                "help", "help", "Show this help message",
                clic_help_cb);

    clic_new_cmd(cmd_lst, "clear", "clear", "Clear terminal", clic_clear_cb);

    clic_new_cmd(cmd_lst, "exit", "exit", "Exit the application", clic_exit_cb);

    clic_prompt(">> ", cmd_lst);

    clic_cmd_list_free(cmd_lst);

    return 0;
}
