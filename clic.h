/**
 * \file clic.h
 * \author Luiz Felipe Feitosa Leite
 * \date 2019
 * \brief Command Line Interface functions, constants and typedefs
 */

#ifndef CLIC_H
#define CLIC_H

#include <readline/readline.h>
#include <readline/history.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define CLIC_ANSI_DEFAULT   "\033[00m"
#define CLIC_ANSI_BOLD      "\033[01m"

#define CLIC_EXIT   -1

// Declared here without initialize to use on the callback
struct clic_cmd_list;

typedef int (*clic_cb) (int argc, 
                        char **argv,
                        struct clic_cmd_list *cmd_list,
                        int idx);

/**
 * \brief Struct to store a command
 */
struct clic_cmd {
    char *name;
    char *usage;
    char *description;
    clic_cb cb;
};

/**
 * \brief Struct to store a list of commands
 */
struct clic_cmd_list {
    struct clic_cmd **cmds;
    unsigned int qty;
};

/**
 * \brief Duplicates a string
 * \param dest String where to copy to
 * \param orig String to be copied
 * \return Copy of orig string
 */
char *clic_strdup (char *dest, char *orig);

/**
 * \brief Frees a vector of arguments
 * \param argc Pointer to the arguments counter
 * \param argv Arguments vector
 */
void clic_free_args (int *argc, char **argv);

/**
 * \brief Adds an argument to a argument vector given a string of the command 
 * line and the count of charcters
 * \param argc Pointer to the arguments counter
 * \param argv Arguments vector
 * \param buffer Command line
 * \param cc Number of characters to copy from the buffer
 * \return The argument vector or NULL if it fails
 */
char **clic_add_arg (int *argc, char **argv, char *buffer, int cc);

/**
 * \brief Parses a command line into a vector of arguments
 * \param buffer The command line
 * \param argc Pointer to the arguments counter
 * \return The argument vector
 */
char **clic_get_cl (char *buffer, int *argc);

/**
 * \brief Initializes a command
 * \return Pointer to the initialized structure or NULL if it fails
 */
struct clic_cmd *clic_cmd_init ();

/**
 * \brief Frees a command
 * \param cmd Command to be freed
 */
void clic_cmd_free (struct clic_cmd *cmd);

/**
 * \brief Set the name property of a command
 * \param cmd Target command
 * \param name Name to save on the command
 * \return Pointer to the name in the command or NULL if it fails
 */
char *clic_cmd_set_name (struct clic_cmd *cmd, char *name);

/**
 * \brief Set the usage property of a command
 * \param cmd Target command
 * \param usage Usage to save on the command
 * \return Pointer to the usage in the command or NULL if it fails
 */
char *clic_cmd_set_usage (struct clic_cmd *cmd, char *usage);

/**
 * \brief Set the description property of a command
 * \param cmd Target command
 * \param description Description to save on the command
 * \return Pointer to the description in the command or NULL if it fails
 */
char *clic_cmd_set_description (struct clic_cmd *cmd, char *description);

/**
 * \brief Set the callback property of a command
 * \param cmd Target command
 * \param cb Callback to save on the command
 */
void clic_cmd_set_cb (struct clic_cmd *cmd, clic_cb cb);

/**
 * \brief Initializes a command list
 * \return Pointer to the initialized commad list or NULL if it fails
 */
struct clic_cmd_list *clic_cmd_list_init ();

/**
 * \brief Frees a command list
 * \param cmd_list Command list to be freed
 */
void clic_cmd_list_free (struct clic_cmd_list *cmd_list);

/**
 * \brief Copies a command
 * \param dest Destination of the copy
 * \param orig Command to be copied
 * \return Pointer to the new copy or NULL if it fails
 */
struct clic_cmd *clic_cmd_cpy (struct clic_cmd *dest, struct clic_cmd *orig);

/**
 * \brief Adds a command to a command list
 * \param cmd_list Command list to receive the command
 * \param cmd Command to added to the command list
 * \return Pointer to the command added on the command list or NULL if it fails
 */
struct clic_cmd *clic_add_cmd (struct clic_cmd_list *cmd_list, 
                               struct clic_cmd *cmd);

/**
 * \brief Adds a new command to a command list
 * \param cmd_list Command list to receive the new cmd
 * \param name Name to the command
 * \param usage Usage of the command
 * \param description Description of the command
 * \param cb Callback of the command
 * \return Pointer to the command added on the command list or NULL if it fails
 */
struct clic_cmd *clic_new_cmd (struct clic_cmd_list *cmd_list,
                               char *name,
                               char *usage,
                               char *description,
                               clic_cb cb);

/**
 * \brief Start a command line interface
 * \param prompt Prompt cursor
 * \param cmd_list List of commands
 */
void clic_prompt (char *prompt, struct clic_cmd_list *cmd_list);

/**
 * \brief List the usage of all commands
 * \param argc Arguments counter
 * \param argv Arguments vector
 * \param cmd_list List of arguments
 * \param idx Index of the current command
 * \return 0
 */
int clic_help_cb (int argc,
                  char **argv,
                  struct clic_cmd_list *cmd_list,
                  int idx);

/**
 * \brief Exit the CLI
 * \param argc Arguments counter
 * \param argv Arguments vector
 * \param cmd_list List of arguments
 * \param idx Index of the current command
 * \return -1, or 1 if the arguments were invalid
 */
int clic_exit_cb (int argc,
                  char **argv,
                  struct clic_cmd_list *cmd_list,
                  int idx);

/**
 * \brief Clear the terminal
 * \param argc Arguments counter
 * \param argv Arguments vector
 * \param cmd_list List of arguments
 * \param idx Index of the current command
 * \return 0, or 1 if the arguments were invalid
 */
int clic_clear_cb (int argc,
                   char **argv,
                   struct clic_cmd_list *cmd_list,
                   int idx);

#endif // CLIC_H
